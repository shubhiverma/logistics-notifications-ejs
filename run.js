"use strict";
var ejs = require('ejs');
var http = require('http');

class Template {
	constructor(template_code) {
		let code = template_code;
		if (template_code.indexOf("_") >= 0) {
			code = template_code.substring(0, template_code.indexOf("_"));
		}
		switch(code) {
			case "6133" :
				this._template = require("./templates/merchant-cancel-sms-6133.js");
				this._options = require("./data/merchant-cancel-sms-6133.js");
				break;
			case "699"  :
			    this._template = require("./templates/order-shipped-customer-push-699.js");
			    this._options = require("./data/order-shipped-customer-push-699.js");
			    break;
			case "3059" :
			    this._template = require("./templates/order-shipped-push- paytm-mall-3059.js");
			    this._options = require("./data/order-shipped-push-paytm-mall-3059.js");
			    break;
			case "5534" :
				this._template = require("./templates/OFD_Notification_Mall-5534.js");
				this._options = require("./data/OFD_Notification_Mall-5534.js");
				break;
			case "5623" :
				this._template = require("./templates/OFP_Notification-5623.js");
				this._options = require("./data/OFP_Notification-5623.js");
				break;
			default :
				console.log("Template code was incorrect :(");
				break;
		}
	}
	get template() {
		return this._template.replace(/{{/g, "<%").replace(/}}/g, "%>");
	}
	get options() {
		return this._options;
	}
}
let input = process.env.T_CODE;
var T = new Template(input);

//console.log(T._options instanceof Array);
if (T._options instanceof Array) {
	T._options.forEach((option) => {
		console.log(ejs.render(T.template, option.order))
	});
} else {
	if (input.substring(input.length - 4) === "html") {
		http.createServer(function(request, response) {  
			response.writeHeader(200, {"Content-Type": "text/html"});  
			response.write(ejs.render(T.template, T.options));  
			response.end();  
		}).listen(8001);
	} else {
		console.log(ejs.render(T.template, T.options));
	}
}