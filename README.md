# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repo can be used to test notification templates locally.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone this repo [git clone git@bitbucket.org:shubhiverma/logistics-notifications-ejs.git]
* To run this repo, enter the following command
    T_CODE=6133 node run.js // Here, 6133 is the template code present in run.js. Enter your template id to run your template locally.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Shubhi Verma [shubhi.verma@paytmmall.com]