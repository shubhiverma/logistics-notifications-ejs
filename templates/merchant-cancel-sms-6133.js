module.exports = `{{  if(payments.length == 1 && payments[0].payment_method == 'PAYTM_DIGITAL_CREDIT'){ 
  var refund_text1 = "We have initiated refund of Rs."+payments[1].pg_amount+" to your Paytm Postpaid account";
}else{
if(context && context.refund && context.refund.length ==1  ){
var refund_text1 = "We have successfully initiated a refund of Rs."+context.refund[0].amount;

}else if(context && context.refund && context.refund.length ==2){
var refund_text1 = "We have successfully initiated a refund of Rs."+ (context.refund[0].amount+context.refund[1].amount);

}else{
var refund_text1 = "We have successfully initiated a refund of Rs."+((items[0].custom_text3 && collectableAmount != grandtotal) ? grandtotal-collectableAmount : items[0].max_refund);
}
}
-}}
{{ if((!items[0].custom_text3) || (items[0].custom_text3 && collectableAmount != grandtotal)){ }}Dear Customer, we regret to inform you that your item {{=items[0].name}} couldn't be fulfilled by the seller. {{= refund_text1}}. You can check the refund status here http://ml.p-y.tm/myorders for refund timeline.{{ } }} `;