module.exports = `{{ var DeliveryDate;
    var itemText = (items.length == 1 ? "has" : "have");

    function formatDate(date){
        var a= new Date(date).toString();
        var b=a.split(" ");
        return (b[0]+", "+b[2] +" "+b[1]);
    }

    if(items[0].fulfillment_delivery_date) {
        DeliveryDate = formatDate(items[0].fulfillment_delivery_date);
    } else { throw new Error("Not fetch Delivery date"); }
    -}}
{{if(items[0].isPhysical){
}}Your order {{=items[0].name}} {{ if(items.length > 1 ){ }}and {{=(items.length)-1}} more {{ } }}{{if(items.length == 1 || items.length == 2){ }}item {{ }else{ }}items {{ } }}{{=itemText}} been shipped and will be delivered to you by {{=DeliveryDate}}.{{ }else{ throw new Error("Not Mktplace Order"); } }}`;